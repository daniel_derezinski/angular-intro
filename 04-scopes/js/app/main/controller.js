/**
 * Created by daniel on 29.02.16.
 */
(function () {
  'use strict';

  var app = angular.module('AngularIntro');

  app.controller('FirstCtrl', ['$scope', function ($scope) {
    $scope.message = 'Pierwsza wiadomość';
    $scope.alertMsg = 'To jest alert';

    $scope.showMessage = function () {
      alert($scope.message);
    };

  }]);

  app.controller('SecondCtrl', ['$scope', function ($scope) {
    $scope.message = 'Druga wiadomość';
    $scope.$parent.message = 'Nadpisana wiadomość';

    $scope.msgList = [
      'Lorem ipsum dolor sit.',
      'Impedit, temporibus. Architecto, consequuntur.',
      'Sed voluptates, molestiae incidunt.',
      'Nam explicabo quaerat optio?'
    ];

  }]);

})();
