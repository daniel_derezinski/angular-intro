/**
 * Created by daniel on 29.02.16.
 */
(function () {
  'use strict';

  var app = angular.module('AngularIntro');

  app.controller('ExampleCtrl', ['$scope', function ($scope) {

    //var $well = angular.element(document.querySelector('.container .well')); // bez jquery
    var $well = angular.element('.container .well'); // z jquery

    $well
      .css({
        color: 'blue',
        textDecoration: 'underline'
      })
      .addClass('big-text');

    //var $div = angular.element(document.createElement('div')); // bez jquery
    var $div = angular.element('<div>'); // z jquery
    $div
      .text('Tekst w divie')
      .css({
        backgroundColor: 'gray',
        textTransform: 'uppercase'
      });

    $well.append($div);


  }])
})();
