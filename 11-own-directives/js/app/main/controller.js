/**
 * Created by daniel on 29.02.16.
 */
(function () {
  'use strict';

  var app = angular.module('AngularIntro');

  app.controller('ExampleCtrl', ['$scope', function($scope){

    $scope.team1 = [
      { name: 'Adam Nowak', position: 'WebDeveloper', avatar: 'img/1.png' },
      { name: 'Grzegorz Kowalski', position: 'Front-end Developer', avatar: 'img/2.png' },
      { name: 'Rafał Nijaki', position: 'Back-end Developer', avatar: 'img/3.png' },
    ];

    $scope.teamLeader = { name: 'Marek Pierwszy', position: 'Leader', avatar: 'img/4.png' };

    $scope.team2 = [
      { name: 'Michał Kowal', position: 'WebDeveloper', avatar: 'img/5.png', email: 'michal@britenet.com.pl' },
      { name: 'Arek Suchy', position: 'Front-end Developer', avatar: 'img/6.png', email: 'arek@britenet.com.pl' },
      { name: 'Rafał Nijaki', position: 'Back-end Developer', avatar: 'img/7.png', email: 'rafal@britenet.com.pl' },
    ];

  }]);
})();
