/**
 * Created by daniel on 29.02.16.
 */
(function () {
  'use strict';

  var app = angular.module('AngularIntro');

  app.controller('FirstCtrl', ['$scope', function ($scope) {
    //$scope.error = 'To jest błąd'

    $scope.comment = 'To jest początkowy komentarz';
    $scope.messages = [
      'Pierwsza',
      'Druga',
      'Trzecia'
    ];

    $scope.post = {
      title: 'Krótka lekcja o projektowaniu',
      categories: ['Programowanie', 'Grafika', 'Projektowanie']
    };

    $scope.clearCategories = function () {
      $scope.post.categories = [];
    };

    $scope.addCategories = function () {
      $scope.post.categories = ['Programowanie', 'Grafika', 'Projektowanie'];
    };

  }])
})();
