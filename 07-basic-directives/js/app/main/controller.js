/**
 * Created by daniel on 29.02.16.
 */
(function () {
  'use strict';

  var app = angular.module('AngularIntro');

  app.controller('ExampleCtrl', ['$scope', function ($scope) {
    $scope.message = "To jest wiadomość z kontrolera";
    //$scope.price = 123.123
  }])

})();
