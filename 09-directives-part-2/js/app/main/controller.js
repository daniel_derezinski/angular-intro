/**
 * Created by daniel on 29.02.16.
 */
(function () {
  'use strict';

  var app = angular.module('AngularIntro');

  app.controller('ExampleCtrl', ['$scope', function ($scope) {
    $scope.users = [
      { name: 'Asia', active: true, role: "user", avatar: 'img/1.png' },
      { name: 'Jarek', active: true, role: "user", avatar: 'img/2.png' },
      { name: 'Tomek', active: false, role: "user", avatar: 'img/3.png' },
      { name: 'Maciek', active: true, role: "admin", avatar: 'img/4.png' },
      { name: 'Kasia', active: true, role: "user", avatar: 'img/5.png' },
      { name: 'Rafał', active: false, role: "user", avatar: 'img/6.png' },
      { name: 'Filip', active: true, role: "user", avatar: 'img/7.png' },
      { name: 'Józek', active: true, role: "admin", avatar: 'img/8.png' },
      { name: 'Adam', active: true, role: "user", avatar: 'img/9.png' },
      { name: 'Zuzia', active: true, role: "admin", avatar: 'img/10.png' },
      { name: 'Grzegorz', active: true, role: "user", avatar: 'img/11.png' }
    ];


  }])

})();
