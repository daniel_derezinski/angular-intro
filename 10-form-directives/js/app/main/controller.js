/**
 * Created by daniel on 29.02.16.
 */
(function () {
  'use strict';

  var app = angular.module('AngularIntro');

  app.controller('ExampleCtrl', ['$scope', 'filterFilter', function ($scope, filterFilter) {
    $scope.loginPrompt = false;
    $scope.useBtEmail = false;
    $scope.formSave = false;

    $scope.member = {};

    $scope.specializations = [
      { name: 'ASP.NET', type: 'Programowanie' },
      { name: 'PHP', type: 'Programowanie' },
      { name: 'HTML/CSS', type: 'Programowanie' },
      { name: 'Photoshop', type: 'Grafika' },
      { name: 'Illustrator', type: 'Grafika' },
      { name: 'Premiere', type: 'Video' },
      { name: 'Photoshop', type: 'Video' },
    ];

    $scope.technologies = [
      { name: 'Photoshop', selected: false },
      { name: 'Illustrator', selected: false },
      { name: 'PHP', selected: false },
      { name: 'ASP.NET', selected: false },
      { name: 'JavaScript', selected: false },
      { name: 'HTML/CSS', selected: false },
      { name: 'MySQL', selected: false },
      { name: 'JAVA', selected: false }
    ];

    $scope.accountTypes = [
      'Standard',
      'Optimum',
      'Premium',
      'Premium+'
    ];

    $scope.populateTechs = function () {

      $scope.member.technologies = filterFilter($scope.technologies, { selected: true });

    };
  }])

})();
