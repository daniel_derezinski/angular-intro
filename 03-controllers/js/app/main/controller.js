/**
 * Created by daniel on 29.02.16.
 */
(function () {
  'use strict';

  var app = angular.module("AngularIntro");

  app.controller('FirstCtrl', ['$scope', function ($scope) {
    $scope.message = 'Wiadomość z kontrolera';
  }]);

  app.controller('SecondCtrl', ['$scope', function($scope){
    $scope.message = 'Wiadomość z drugiego kontrolera';
  }]);

  app.controller('SettingsCtrl', ['$scope', function($scope){
    $scope.settings = {
      firstName: 'Jan',
      lastName: 'Kowalski'
    };
  }]);

  app.controller('PasswdCtrl', ['$scope', function($scope){
    $scope.passwdHistory = {
      prev: 'old-password',
      curr: 'current-password'
    };
  }]);

  app.controller('VeryLongNameCtrl', function(){
    this.message = 'Bardzo długa wiadomość z kontrolera';
  });

})();
