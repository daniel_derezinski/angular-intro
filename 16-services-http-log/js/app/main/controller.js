/**
 * Created by daniel on 29.02.16.
 */
(function () {
  'use strict';

  var app = angular.module('AngularIntro');

  app.controller('ExampleCtrl', ['$scope', '$http', '$log', function ($scope, $http, $log) {

    $http.get('http://localhost:3000/users/').then(function (response) {
      $log.info('success', response);
      $scope.team1 = response.data;

    }, function (response) {
      $log.error('error', response);

    });

    $scope.team1 = []


  }])
})();
