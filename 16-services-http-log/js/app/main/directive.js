/**
 * Created by daniel on 01.03.16.
 */
(function () {
  'use strict';

  var app = angular.module("AngularIntro");

  app.directive('businessCard', function () {
    return {
      scope: {
        member: '='
      },
      restrict: 'AEC',
      //template: '<div class="well text-center bc-style"><img ng-src="{{ member.avatar }}" alt=""><h4>{{ member.name }}</h4><h6>{{ member.position }}</h6></div>'
      //templateUrl: 'member-simple-bc.html'
      templateUrl: function (elem, attr) {
        attr.type = attr.type||'simple';
        return 'member-'+attr.type+'-bc.html';
      }
    };

  });
})();
