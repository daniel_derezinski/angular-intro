/**
 * Created by daniel on 01.03.16.
 */
(function () {
  'use strict';

  var app = angular.module('AngularIntro');

  app.filter('maskEmail', function () {
    return function (email, length, signMask) {
      email = email || "";
      length = length || 3;
      signMask = signMask || '*';
      var parts = email.split('@');
      var masked = parts[0].substr(0, length);
      var maskLength = parts[0].length - length;

      for (var i=0; i < maskLength; i++) {
        masked += signMask;
      }

      parts[0] = masked;

      return parts.join('@');
    }
  });

  app.filter('joiner', function () {
    var history = [];

    return function (input, sep) {
      sep = sep || ', ';
      history.unshift(input);

      return history.join(sep)
    }
  })
})();
