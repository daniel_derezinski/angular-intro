/**
 * Created by daniel on 29.02.16.
 */
(function () {
  'use strict';

  var app = angular.module('AngularIntro');
  app.controller('ExampleCtrl', ['$scope', 'orderByFilter', 'maskEmailFilter', "$filter",
    function($scope, orderByFilter, maskEmailFilter, $filter){
    //$scope.users = ['Adam', 'Aneta', 'Marek', 'Kasia', 'Juzek', 'Antek', 'Filip', 'Asia'];

    $scope.users = [
      { name: 'Adam', email: 'adam@britenet.com.pl' },
      { name: 'Aneta', email: 'aneta@britenet.com.pl' },
      { name: 'Marek', email: 'marek@britenet.com.pl' },
      { name: 'Kasia', email: 'kasia@britenet.com.pl' },
      { name: 'Juzek', email: 'juzek@britenet.com.pl' },
      { name: 'Antek', email: 'antek@britenet.com.pl' },
      { name: 'Filip', email: 'filip@britenet.com.pl' },
      { name: 'Asia', email: 'asia@britenet.com.pl' }
    ];

    $scope.orderUsers = function () {

      //sortowanie użytkowników
      console.log('sortowanie użytkowników');
      $scope.users = orderByFilter($scope.users, 'name');
    };

    $scope.hideEmail = function () {

      //ukrywanie haseł
      console.log('ukrywanie haseł');
      var users = $scope.users;

      for (var i = 0; i < users.length; i++) {
        users[i]['email'] = maskEmailFilter(users[i]['email'], 1, '.');
      }

      $scope.users = users

    };

    $scope.showTop3 = function () {

      //pokaż Top3
      console.log('pokaż top3');
      $scope.users = $filter('limitTo')($scope.users, 3);

    };
  }]);
})();
