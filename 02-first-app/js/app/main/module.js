/**
 * Created by daniel on 29.02.16.
 */
(function () {
  'use strict';

  var app = angular.module('AngularIntro', []);

  app.controller('AngularIntroCtrl', ['$scope', 'userProvider', function($scope, userProvider){

    $scope.comment = '...dodaj komentarz...';

    $scope.shoppingCart = [
      { name: 'Sekrety Photoshop - Tips & Tricks', price: 99 },
      { name: 'ASP.NET MVC - w Praktyce', price: 149 },
      { name: 'Sekrety Photoshop - Tips & Tricks', price: 99 },
      { name: 'Kreacje 3D w Modo Z Photoshop, Illustrator i ZBrush', price: 129 }
    ];

    $scope.shippingPrice = 14;

    $scope.userName = userProvider.fullName();

    $scope.confirmOrder = function () {
      alert('Dziękujemy! Twoje zamówienie zostało przyjęte!');
      console.log('Komentarz: ' + $scope.comment);
      console.log('Produktów: ' + $scope.shoppingCart.length);
    };

    $scope.clearCart = function () {
      $scope.shoppingCart = [];
    };

    $scope.removeFromCart = function (id) {
      $scope.shoppingCart.splice(id, 1);
    };

  }]);

  app.factory('userProvider', function(){
    return {
      firstName: 'Maciej',
      lastName: 'Żukiewicz',
      email: 'maciek@eduweb.pl',
      fullName: function () {
        return this.firstName + ' ' + this.lastName;
      }
    };
  });

})();
