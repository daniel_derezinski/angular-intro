/**
 * Created by daniel on 29.02.16.
 */
(function () {
  'use strict';

  var app = angular.module('AngularIntro');

  app.controller('ExampleCtrl', ['$scope', function ($scope) {
    $scope.shapeA = {
      name: 'Object A',
      width: 100,
      height: 200,
      subObj: {
        subName: 'subObject A',
        subParam: 'subParam A'
      },
      show: function () {
        return this.name + ' (' + this.width + 'x' + this.height + ' / ' + this.subObj.subName + ', ' + this.subObj.subParam + ')';
      }
    };

    //$scope.shapeB = $scope.shapeA;
    $scope.shapeB = angular.copy($scope.shapeA);

    $scope.shapeB.name = "Object B";

    $scope.shapeB.subObj.subName = 'subObject B';
    $scope.shapeB.subObj.subParam = 'subParam B';

    console.log(angular.equals($scope.shapeA, $scope.shapeB));

    var defaultOpts = {
      backgroundColor: 'red',
      fontSize: '13px',
      color: 'white'
    };

    var optionsA = {
      fontSize: '16px'
    };

    var optionsB = {
      width: 100,
      height: 150,
    };

    $scope.finalOpt = angular.extend(defaultOpts, optionsA, optionsB);

    console.log(defaultOpts);

    var users = [
      { firstName: 'Adam', lastName: 'Nowak' },
      { firstName: 'Krystyna', lastName: 'Kowalska' },
      { firstName: 'Michał', lastName: 'Anioł' },
      { firstName: 'Johny', lastName: 'Rambo' },
      { firstName: 'Rocky', lastName: 'Balboa' },
      { firstName: 'Żółw', lastName: 'Ninja' },
      { firstName: 'Bruce', lastName: 'Lee' }
    ];

    var simpleUsers = []; // '0. Adam Nowak', '1. Krystyna Kowalska'

    angular.forEach(users, function (value, key) {
      //simpleUsers.push(key+'. '+value.firstName+' '+value.lastName);
      this.push(key+'. '+value.firstName+' '+value.lastName);
    }, simpleUsers);

    console.log(simpleUsers);

  }])
})();
