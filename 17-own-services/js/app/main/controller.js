/**
 * Created by daniel on 29.02.16.
 */
(function () {
  'use strict';

  var app = angular.module('AngularIntro');

  app.controller('ExampleCtrl', ['$scope', 'memberLoader', 'teamNotifier',
    function ($scope, memberLoader, teamNotifier) {


      $scope.team1 = [];
      $scope.team2 = [];
      $scope.teamLeader = {};

      memberLoader('team1', function (response) {
        $log.info('success team1');
        $scope.team1 = response.data;
      });

      memberLoader('team2', function (response) {
        $log.info('success team2');
        $scope.team2 = response.data;
      });

      memberLoader('leader', function (response) {
        $log.info('success team leader');
        $scope.teamLeader = response.data;
      });

      $scope.notifyTeam = function (team) {
        teamNotifier(team, 'Do roboty!!!');
      }

    }]);
})();
