/**
 * Created by daniel on 02.03.16.
 */
(function () {
  'use strict';

  var app = angular.module('AngularIntro');

  app.factory('memberLoader', ['$http', '$log', function ($http, $log) {
    var urls = {
      team1: "http://localhost:3000/users/team1",
      team2: "http://localhost:3000/users/team2",
      leader: 'http://localhost:3000/users/leader'
    };

    return function (type, callback) {
      var url = urls[type];
      callback = callback || function () {};

      $http.get(url)
        .then(callback, function (response) {
          $log.error('error ' + type, response)
        });
    }
  }]);

  app.factory('teamNotifier', ['$log', function ($log) {

    var notifyMember = function (member, message) {
      $log.info('Wysyłam wiadomośc "' + message + '" do ' + member.name + ' (' + member.email + ')');
    };

    return function (team, message) {
      for (var i = 0; i < team.length; i++) {
        notifyMember(team[i], message);
      }
    }
  }])

})();
