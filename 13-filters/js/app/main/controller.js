/**
 * Created by daniel on 29.02.16.
 */
(function () {
  'use strict';

  var app = angular.module('AngularIntro');
  app.controller('ExampleCtrl', ['$scope', function ($scope) {
    $scope.price = 456789.123
    $scope.currentDate = new Date();
    $scope.posts = [{title:'post1'}, {title:'post2'}, {title:'post3'}]
  }])
})();
